const express = require("express");
const mongoose = require("mongoose");
require('dotenv').config();
const cors = require("cors");

// routes
const userRoutes = require("./routes/userRoutes");
const productRoutes = require("./routes/productRoutes")

// server
const app = express()

// resources
app.use(cors());
app.use(express.json());
app.use(express.urlencoded({  extended:true }));

// connection for MongoDB
mongoose.connect(process.env.DB_CONNECTION, {
	useNewUrlParser: true,
	useUnifiedTopology: true
})


app.use("/users", userRoutes);
app.use("/products", productRoutes)

mongoose.connection.once('open', () => console.log('Now connected to MongoDB Atlas'));

app.listen(process.env.PORT, () => {
	console.log(`API is now online on port ${process.env.PORT}`)
})
