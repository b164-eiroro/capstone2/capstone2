const mongoose = require("mongoose")

const productSchema = new mongoose.Schema({
	name: {
		type: String,
		required:[true, "Product is required."]
	},
	description: {
		type: String,
		required: [true, "Description is required."]
	},
	price: {
		type: Number,
		required: [true, "Price is required."]
	},
	department: {
		type: String,
		required: [true, "Department is required"]
	},
	isAvailable: {
		type: Boolean,
		default: true
	}

})

module.exports = mongoose.model("Product", productSchema);