const User = require("../models/User");
//encrypted password
const bcrypt = require('bcrypt');

const auth = require("../auth");

module.exports.registerUser = (reqBody) => {
	let newUser = new User ({
		firstName: reqBody.firstName,
		lastName: reqBody.lastName,
		userName: reqBody.userName,
		mobileNo: reqBody.mobileNo,
		email: reqBody.email,
		password: bcrypt.hashSync(reqBody.password, 10),
		isAdmin: reqBody.isAdmin
	})

	return newUser.save().then((user, err) => {
		if (err) {
			return false
		} else {
			return true
		}
	})

}

module.exports.loginUser = (reqBody) => {

	return User.findOne({ userName: reqBody.userName }).then(result => {
		//User does not exist
		if(result == null){
			return false;
		} else {
			const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password)

			if(isPasswordCorrect){
				return { accessToken : auth.createAccessToken(result.toObject())}
			} else {
				return false;
			}

		};
	});
};



module.exports.setAdmin = (reqBody) => {

	let updatedIsAdmin = {
		isAdmin: true
	}

	return User.findByIdAndUpdate(reqBody, updatedIsAdmin).then((product, error) => {
		if(error) {
			return false;
		} else {
			return true;
		}
	})
}

module.exports.order = async (data) => {
	let isUserVerified = await User.findById(data.userId).then(user => {
		user.orders.push({ productId: data.productId})
		return user.save().then((user, err) => {
			if (err) {
				return false
			} else {
				return true
			}
		})
	})
	if (isUserVerified) {
		return true
	} else {
		return false
	}

}
module.exports.allOrders = (reqBody) => {
	return User.find({isAdmin: false}).select('orders').then(result => {return result})
}

module.exports.userOrders = (reqParams) => {
	return User.findById(reqParams).select('orders').then(result => {return result})
}