const Product = require("../models/Product");

module.exports.addProduct = (reqBody) => {
	let newProduct = new Product ({
		name: reqBody.product.name,
		description: reqBody.product.description,
		price: reqBody.product.price,
		department: reqBody.product.department

	})
	return newProduct.save().then((product, err) => {
		if (err) {
			return false
		} else {
			return true
		}
	})
}

module.exports.allProducts = () => {
	return Product.find({ isAvailable: true }).then( result => {return result})
}

module.exports.getProduct = (reqParams) => {
	return Product.findById(reqParams).then(result => {return result})
}

module.exports.updateProduct = (productId, reqBody) => {
	let updatedProduct = {
		name: reqBody.product.name,
		description: reqBody.product.description,
		price: reqBody.product.price,
		department: reqBody.product.department

	}
	return Product.findByIdAndUpdate(productId, updatedProduct).then((product, err) => {
		if (err) {
			return false
		} else {
			return true
		}
	})
}

module.exports.archiveProduct = (productId) => {
	let updateAvailable = {
		isAvailable: false
	}
	return Product.findByIdAndUpdate(productId, updateAvailable).then((product, err) => {
		if (err) {
			return false
		} else {
			return true
		}
	})
}