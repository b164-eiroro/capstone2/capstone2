const express = require("express");
const router = express.Router();
const auth = require("../auth");
const ProductController = require("../controllers/productControllers");

router.post("/", auth.verify, (req, res) => {
	const data = {
		product: req.body,
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}

	if(data.isAdmin){
	ProductController.addProduct(data).then(result => res.send(result))
	} else {
		res.send("You're not an admin!")
	}

})

router.get("/all", (req, res) => {
	ProductController.allProducts().then(result => res.send(result))
})

router.get("/:productId", (req, res) => {
	ProductController.getProduct(req.params.productId).then(result => res.send(result))
})

router.put("/update/:productId", auth.verify, (req, res) => {
	const data = {
		product: req.body,
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}
	if (data.isAdmin) {
		ProductController.updateProduct(req.params.productId, data).then(result => res.send(result))
	} else {
		res.send("You're not an admin!")
	}
})

router.put("/archive/:productId", auth.verify, (req, res) => {
	const data = {
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}
	if (data.isAdmin) {
		ProductController.archiveProduct(req.params.productId).then(result => res.send(result))
	} else {
		res.send("You're not an admin!")
	}
})
module.exports = router;