const express = require("express");
const router = express.Router();
const auth = require("../auth");

const UserController = require("../controllers/userControllers");


router.post("/register", (req, res) => {
	UserController.registerUser(req.body).then(result => res.send(result))
})

router.post("/login", (req, res) => {
	UserController.loginUser(req.body).then(result => res.send(result));
});

router.put('/admin/:userId',auth.verify, (req, res) =>{
	const data = {
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}

	if(data.isAdmin){
		UserController.setAdmin(req.params.userId).then(result => res.send(result))
	} else {
		res.send("You're not an admin!")
	}

})
router.post("/addtocart", auth.verify, (req, res) => {
	let data = {
		userId : auth.decode(req.headers.authorization).id,
		productId : req.body.productId
	}

	UserController.order(data).then(result => res.send(result));
	
	
})

router.get("/orders", auth.verify, (req, res) => {
	const data = {
		isAdmin: auth.decode(req.headers.authorization).id
	}
	if (data.isAdmin) {
		UserController.allOrders().then(result => res.send(result))
	} else {
		res.send("You're not an admin!")
	}
})


router.get("/:userId/orders", auth.verify, (req, res) => {
	const data = {
		userId: auth.decode(req.headers.authorization).id
	}

	if(data.userId){
		UserController.userOrders(req.params.userId).then(result => res.send(result))
	} else {
		res.send("You're not a user!")
	}

})
module.exports = router;